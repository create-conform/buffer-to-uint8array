/////////////////////////////////////////////////////////////////////////////////////////////
//
// buffer-to-uint8array
//
//    Converts node.js Buffer to Uint8Array.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Polyfill
//
/////////////////////////////////////////////////////////////////////////////////////////////
if (typeof Buffer != "undefined" && typeof Buffer.prototype.toUint8Array == "undefined") {
    Buffer.prototype.toUint8Array = function() { 
        if (this.buffer)
        { 
            return new Uint8Array(this.buffer, this.byteOffset, this.byteLength);
        } 
        else { 
            var ab = new ArrayBuffer(this.length);
            var view = new Uint8Array(ab);
            for (var i = 0; i < this.length; ++i) { 
                view[i] = this[i];
            } 
            return view;
        } 
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = Buffer;